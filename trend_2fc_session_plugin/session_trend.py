# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging

from pyforms import conf

import numpy as np

from AnyQt.QtWidgets import QMessageBox

from AnyQt.QtGui import QColor, QBrush
from AnyQt.QtCore import QTimer, QEventLoop, QAbstractTableModel, Qt, QSize, QVariant, pyqtSignal

from pybpodapi.session import Session
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlProgress
from pyforms.controls import ControlText
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlLabel
from pyforms.controls import ControlButton
from pyforms.controls import ControlCheckBox

from matplotlib.lines import Line2D

from trend_2fc_session_plugin.myData import strToData

import logging
import traceback
import datetime

import os

logger = logging.getLogger(__name__)


class SessionTrend(BaseWidget):
    """ Plugin main window """

    COUNT = 0 # Counter to rebember the index of data of the query.

    def __init__(self, session):
        BaseWidget.__init__(self, session.name)

        self.layout().setContentsMargins(5, 5, 5, 5) # Set graphic margin
        self.session = session # Session Class of the pybpod to get data

        # Form creation
        self._graph = ControlMatplotlib('Graph')
        self._updatebutton = ControlButton('Update')
        self._valGraph = ControlText('Values to show. [-1 for all]: ', default='100', helptext='Values that are being plotted in the graph.')
        self._titleTrend = ControlLabel('Trend of 2FC task.')
        self._progress = ControlProgress('Loading', 0, 1, 100)
        self._coh_checkbox = ControlCheckBox('Show coherences')

        # Form position
        self._formset = [
            ('_titleTrend', '_valGraph','_updatebutton', '_coh_checkbox'),
            '_graph',
            '_progress'
        ]

        # Assign an action to the button
        self._updatebutton.value = self.__updateAction

        # Class containing all the data     
        self._dt = strToData()

        # Set Property 
        self._progress.hide() # hide the progession bar
        self._graph.value = self.__on_draw # define the action to the graph.

        # Set the timer to recall the function update
        self._timer = QTimer()
        self._timer.timeout.connect(self.update)
        
        # Vector to contain the choices vector that will be extract from the data
        self.variaiables = None  

        try:
            self._setup_name = self.session.data[self.session.data.MSG == 'SETUP-NAME']['+INFO'].iloc[0]
        except IndexError:
            self._setup_name = "Unknown box"
        try:
            self._session_started = self.session.data[self.session.data.MSG == 'SESSION-STARTED']['+INFO'].iloc[0].split()[1][0:8]
        except IndexError:
            self._session_started = "Unknown time"
        
        try:
            self._subject_name = self.session.data[self.session.data.MSG == 'SUBJECT-NAME']['+INFO'].iloc[0]
            self._subject_name = self._subject_name[1:-1].split(',')[0].strip("'")
        except IndexError:
            self._subject_name = "Unknown subject"
        

        self.title = f'{self._setup_name} / {self._session_started} / {self._subject_name}'

    def __updateAction(self):
        """ Action Button """
        # Check the value and update the graph
        try:
            v = int(self._valGraph.value) # transform the string value to int
            if v > 0 or v == -1: # it's ok only in this case
                self._dt.MAXVAL_TOSHOW = v
                self._graph.draw()
            else: # otherwise show poupup message and reset the value
                self.warning('Only integer grater than zero or -1 if you want to show all the data.', 'ValueError')
                self._valGraph.value = str(self._dt.MAXVAL_TOSHOW)
        except Exception:
            self.warning('Only integer grater than zero or -1 if you want to show all the data.', 'ValueError')

    def read_message_queue(self, update_gui = False):
        """ Update data and the graph """
        try:
            new_reward = []
            if self.variaiables is None: # if the vector choice was not set
                self.variaiables = self.session.data[self.session.data.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1]
                self._dt.add(self.variaiables) # Pass it to the myData

            data = self.session.data[self.COUNT:] # take care only the data unread till now
            self.COUNT += len(data) # update the counter

            coh = list(map(float,data.query("MSG == 'coherence01'")['+INFO'].values)) # gather coherences
            evi = [-(2 * elem - 1) for elem in coh] # translate coherences to evidences
            inv = list(map(float, data.query("TYPE=='{0}' and MSG=='Invalid'".format(Session.MSGTYPE_STATE))['BPOD-INITIAL-TIME'].values)) # Find the Invalid states
            res = list(map(float, data.query("TYPE=='{0}' and MSG=='Reward'".format(Session.MSGTYPE_STATE))['BPOD-INITIAL-TIME'].values)) # Find the Reward states
            miss = list(map(float, data.query("TYPE=='{0}' and MSG=='Miss'".format(Session.MSGTYPE_STATE))['BPOD-INITIAL-TIME'].values)) # Find the Invalid states
            new_reward_side = data.query("TYPE=='VAL' and MSG=='REWARD_SIDE'")['+INFO'].values #str

            if new_reward_side.size:
                a = new_reward_side[0][1:-1].split(',')
                self._dt.add_reward_side(a)

            n_results = len(res)
            if not inv: inv = [np.nan] * n_results
            if not miss: miss = [np.nan] * n_results

            values = list(zip(inv, miss, res))
            print(values)

            if update_gui: # it is True only one time: take a look at the show() method
                self._progress.show()
                self._progress.value = 0
                self._progress.max = len(res)
            if evi:
                for elem in evi:
                    self._dt.add_coherences(elem)
            for elem in values: # Parsing of the Reward states
                if np.isnan(elem[0]):
                    if np.isnan(elem[1]):
                        if np.isnan(elem[2]):
                            self._dt.answare(-1) # wrong answer    
                        else:
                            self._dt.answare(1) # right answer  
                    else:
                        self._dt.answare('m')
                else: #invalid trial
                    self._dt.answare('x')
                if update_gui: 
                    self._progress += 1
                QEventLoop()
            self._graph.draw()

        except Exception as err:
            if hasattr(self, '_timer'):
                self._timer.stop()
            # logger.error(str(err), exc_info=True)
            traceback.print_tb(err.__traceback__)
            with open('plugins_logs.txt', 'a') as logfile:
                    logfile.write(str(datetime.datetime.now())+'\n')
                    logfile.write(traceback.format_exc())
                    logfile.write('-'*25)
            QMessageBox.critical(self, "Error",
                                 "An error in the trend plugin occurred. Check logs for details.")

        if update_gui: # at the end Hide the bar
            self._progress.hide()

    def update(self):
        """ This method is called many times until the session is stopped """
        if not self.session.is_running: self._timer.stop()
        if len(self.session.data) > self.COUNT:
            self.read_message_queue()

    def __on_draw(self, figure):
        """ Redraws the figure """
        try:
            # Clean the figure, and create 2 subplot
            figure.clear()
            axes1 = figure.add_subplot(211)
            axes2 = figure.add_subplot(212)
            # Figure 1
            if self._dt._coherences and self._coh_checkbox.value:
                axes1.set_yticks([-1,1], minor = False)
                axes1.set_yticks([0], minor = True)
                axes1.grid(which='minor', linestyle = 'dashed', linewidth = 1, color = 'black', alpha = 0.6)
                axes1.set_yticklabels(['R', 'L'])
                p, reward_side = self._dt.ask_data_subset()
                x, coh, color = self._dt.ask_coherence_subset()
                for i, elem in enumerate(coh):
                    if int(elem) == 0: 
                        if reward_side[i] == -1:
                            coh[i] = elem - 0.02                   
                        else: 
                            coh[i] = elem + 0.02
                axes1.scatter([elem+1 for elem in x], coh, marker = 'o', c = color, s = 10)
                axes1.scatter(p[len(coh):], reward_side[len(coh):], c = 'gray', marker = 'o', s = 10)
            else:
                x, y = self._dt.ask_data_subset()
                axes1.scatter(x, y, s = 10, c = self._dt.data_answare)
                axes1.set_yticks([-1,1])
                axes1.set_yticklabels(['R','L'])
            axes1.set_ylim([-1.1,1.1])
            axes1.grid(which='major', linestyle = 'dashed', alpha = 0.6)
            if x:
                axes1.set_xlim([self._dt.iStart, self._dt.iEnd])
                axes2.set_xlim([self._dt.iStart, self._dt.iEnd])
            # Figure 2
            axes2.set_ylabel('Accuracy [%]')
            axes2.set_xlabel('Trials')
            perf_L, perf_R = self._dt.ask_performances_subset()
            axes2.scatter([elem[0]+1 for elem in self._dt._performance], [elem[1] for elem in self._dt._performance], facecolors='none', edgecolors='black', s =15, alpha = 0.9)
            axes2.scatter([elem[0]+1 for elem in perf_L], [elem[1] for elem in perf_L], facecolors='none', edgecolors='cyan', s = 15, alpha = 0.9)
            axes2.scatter([elem[0]+1 for elem in perf_R], [elem[1] for elem in perf_R], facecolors='none', edgecolors='magenta', s = 15, alpha = 0.9)
            axes2.plot([1,2,3,4,5], [0,0,0,0,0], alpha = 0)
            axes2.set_ylim([0,1.1])
            axes2.set_yticks(list(np.arange(0,1.1, 0.1)))
            axes2.set_yticklabels(['0', '', '','','','50', '','','','','100'])
            axes2.grid(linestyle = 'dashed', alpha = 0.6)

            figure.tight_layout()

            legend_elements = [Line2D([0], [0],color='w', marker='o',markersize=5, markerfacecolor="white", markeredgecolor = "black", label='Total performance'),
            Line2D([0], [0], marker='o', color='w', markersize=5, markerfacecolor="white", markeredgecolor = "cyan", label='Left channel'),
            Line2D([0], [0], marker='o', color='w',markersize=5, markerfacecolor="white", markeredgecolor = "magenta", label='Right channel')]
            axes2.legend(bbox_to_anchor=(1,0), loc="lower right", bbox_transform=figure.transFigure,handles=legend_elements, ncol=3)
            
        except Exception as err:
            print(err)

    def show(self, detached=False):
        """ Method used to show the windows """
        # Prevent the call to be recursive because of the mdi_area
        if not detached:
            if hasattr(self, '_show_called'):
                BaseWidget.show(self)
                return
            self._show_called = True
            self.mainwindow.mdi_area += self
            del self._show_called
        else:
            BaseWidget.show(self)
        
        self._stop = False  # flag used to close the gui in the middle of a loading
        self.read_message_queue(True) # Read all the data and update the plot
        
        if not self._stop and self.session.is_running: # if it is in real time
            self._timer.start(conf.SESSIONTREND_PLUGIN_REFRESH_RATE)

    def before_close_event(self):
        """ Method called before to close the windows """
        self._timer.stop()
        self._stop = True
        self.session.sessiontrend_action.setEnabled(True)
        self.session.sessiontrend_detached_action.setEnabled(True)

    @property
    def mainwindow(self):
        return self.session.mainwindow

    @property
    def title(self):
        return BaseWidget.title.fget(self)

    @title.setter
    def title(self, value):
        BaseWidget.title.fset(self, value)

