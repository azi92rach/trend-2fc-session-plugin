import numpy as np


class strToData:
    
    COLOR_CORRECT = 'green'
    COLOR_WRONG   = 'red'
    COLOR_NEXT    = 'blue'
    COLOR_INVALID = 'gray'
    COLOR_MISS = 'black'
    MAXVAL_TOSHOW = 100

    def __init__(self):
        self._x                    = [] # x axes
        self._data                 = [] # It collects the right and left choices, that is, _data = VECTOR_CHOICE
        self._data_answare         = [] # It collects 0 or 1 if the animal make correct (green) or not (red)
        self._data_perfomance      = [] # 1 if correct, 0 not 
        self._performance          = [] # means of data_performance as a list of tuples (index, value)
        self._performance_R        = [] # performance of the right side as a list of tuples (index, value)
        self._performance_L        = [] # performance of the left side as a list of tuples (index, value)
        self._performance_L_mean   = [] # mean performance of the right side as a list of tuples (index, mean value)
        self._performance_R_mean   = [] # mean performance of the left side as a list of tuples (index, mean value)
        self._coherences           = [] # contains the trial coherences

        # index used as vector position 
        self.index = 0

        # indexes used to retunr a windows of data
        self.iStart = 0
        self.iEnd   = 0 

    def compute_performance_window(self):
        """ Computes the combined performance of left and right channels as a rolling average of width = 20 samples """
        #print(self._data_perfomance)
        if self.index < 21: #first 20 samples; index starts at 0 here
            self._performance.append((self.index, np.mean(self._data_perfomance)))
        else:
            self._performance.append((self.index, np.mean(self._data_perfomance[-20:-1])))

    def compute_performance(self, side):
        """ Computes the performance of left and right channels as a rolling average of width = 20 samples """
        if side == "R":
            if len(self._performance_R) < 21: #first 20 samples of the right side
                self._performance_R_mean.append((self.index, np.mean([elem[1] for elem in self._performance_R])))
            else:
                self._performance_R_mean.append((self.index, np.mean([elem[1] for elem in self._performance_R[-20:-1]])))
        else:
            if len(self._performance_L) < 21: #first 20 samples of the left side
                self._performance_L_mean.append((self.index, np.mean([elem[1] for elem in self._performance_L])))
            else:
                self._performance_L_mean.append((self.index, np.mean([elem[1] for elem in self._performance_L[-20:-1]])))

    def answare(self, answ):
        """ (-1) = Wrong answer, 1 = Correct answer, 'x' = invalid trial """
        if len(self._data_answare) != 0:
            if answ != 'x':
                if answ == 1:
                    self._data_answare[self.index] = self.COLOR_CORRECT
                    self._data_perfomance.append(1) # save the performance
                elif answ == -1:
                    self._data_answare[self.index] = self.COLOR_WRONG
                    self._data_perfomance.append(0) # save the performance
                elif answ == 'm':
                    self._data_answare[self.index] = self.COLOR_MISS
                    self._data_perfomance.append(0) # save the performance
                if self._data[self.index] == -1:
                    if answ in ('m', -1): #right side, wrong answer
                        self._performance_R.append((self.index, 0))
                    else: #right side, correct answer
                        self._performance_R.append((self.index, 1))
                    self.compute_performance('R')
                else:
                    if answ in ('m', -1): #left side, wrong answer
                        self._performance_L.append((self.index, 0))
                    else: #left side, correct answer
                        self._performance_L.append((self.index, 1))
                    self.compute_performance('L')
                # Compute the mean performance until now
                self.compute_performance_window()
            else: # this was an invalid trial
                self._data_answare[self.index] = self.COLOR_INVALID

            self.index += 1

            if self.index < len(self._data_answare): # Color Blue the next point
                self._data_answare[self.index] = self.COLOR_NEXT 
    
    def add_coherences(self, coh):
        self._coherences.append(coh)

    def add(self, value, color = 'gray'):
        """ Add the vector of choices and set the others vectors"""
        extr_int = value[1:-1].split(',')
        # change to -1 convention:
        self._data = [1 if elem == ' 0' or elem == '0' else -1 for elem in extr_int]
        self._data_answare = [color] * len(self._data)
        self._data_answare[self.index] = self.COLOR_NEXT
        self._x = range(1, len(self._data)+1)

    def add_reward_side(self, reward_side):
        """ Function that updates reward_side if it is found inside the new data query """
        a = [1 if elem == ' 0' or elem == '0' else -1 for elem in reward_side]
        self._data = a

    def __check_idx(self):
        """ set the indexes for the windows """
        len_data = len(self._data)
        if len_data > 0:
            if len_data < self.MAXVAL_TOSHOW or self.MAXVAL_TOSHOW == -1: 
                # Show all
                self.iStart = 0
                self.iEnd   = len_data
            else:
                if self.index > self.MAXVAL_TOSHOW/2 and self.index < (len_data-self.MAXVAL_TOSHOW/2):
                    # Show only MAXVAL_TOSHOW values and moving the window
                    self.iStart = self.index - int(self.MAXVAL_TOSHOW/2)
                    self.iEnd   = self.index + int(self.MAXVAL_TOSHOW/2)+1
                elif self.index <= self.MAXVAL_TOSHOW/2:
                    # First MAXVAL_TOSHOW
                    self.iStart = 0
                    self.iEnd   = self.MAXVAL_TOSHOW
                elif self.index >= (len_data-self.MAXVAL_TOSHOW/2):
                    # Lasr MAXVAL_TOSHOW
                    self.iStart = len_data - self.MAXVAL_TOSHOW
                    self.iEnd   = len_data

    def ask_data_subset(self):
        """ Returns the data indices and the data itself for the plot"""
        self.__check_idx()
        return self._x[self.iStart:self.iEnd], self._data[self.iStart:self.iEnd]

    def ask_performances_subset(self):
        """ Returns the necessary indices and performance data for the plot """
        perf_L = []
        perf_R = []
        for counter, elem in enumerate(self._performance_L_mean):
            if elem[0] >= self.iStart:
                perf_L = self._performance_L_mean[counter:self.iEnd]
                break
        for counter, elem in enumerate(self._performance_R_mean):
            if elem[0] >= self.iStart:
                perf_R = self._performance_R_mean[counter:self.iEnd]
                break
        return perf_L, perf_R

    def ask_coherence_subset(self):
        self.__check_idx()
        coherence_subset = self._coherences[self.iStart:]
        return range(self.iStart, self.iStart + len(coherence_subset)), coherence_subset, self._data_answare[self.iStart:self.iStart + len(coherence_subset)]

    @property
    def data_answare(self):
        """ Return the following vector """
        self.__check_idx()
        return self._data_answare[self.iStart:self.iEnd]

    @property
    def performance(self): 
        """ Return the following vector """
        for counter, elem in enumerate(self._performance):
            if elem[0] >= self.iStart:
                return self._performance[counter:-1]
    
